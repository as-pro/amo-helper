<?php

use ASPRO\Amo\Manager;
use ASPRO\Amo\Models\Contact;

require __DIR__.'/../vendor/autoload.php';

$accounts = json_decode(file_get_contents(__DIR__ . '/../.amo_accounts.json'), true);
$acc = $accounts['zkfpvvml'];

$manager = new Manager($acc['domain'], $acc['login'], $acc['api_key'], null, true);

$adapter = new \Symfony\Component\Cache\Adapter\FilesystemAdapter();
$cache = new \Symfony\Component\Cache\Psr16Cache($adapter);

$manager->setCache($cache);

$contact = $manager->findContactByEmail('email@example.ru')
           ?? new Contact();

$contact->addPhones(['8-999-111-22-33']);
$contact->addEmails(['email@example.ru', 'dddd@fff.ru']);

$contact->setCustomField('кф_переключатель', 'я');
$contact->setCustomField('кф_список', 'а');
$contact->setCustomField('кф_мультисписок', 'вп');
$contact->setCustomField('кф_чисо', 123456);
$contact->setCustomField('кф_флаг', true);
$contact->setCustomField('кф_дата', time());
$contact->setCustomField('кф_текст_обл', 'Большой текст');
$contact->setCustomField('кф_ссылка', 'http://example.com');
$contact->setCustomField('кф_текст', 'Простой текст');
$contact->setCustomField('кф кор адрес', 'Москва, ул.Московская, д.123, кв.456');

$manager->save($contact);

$deal = $manager->getLastActiveDealForContact($contact)
        ?? $contact->newDeal();


$manager->save($deal);
$manager->link($contact, $deal);

$note = $deal->newNote('примечание');
$task = $deal->newTask('задача');

$manager->save([$note, $task]);