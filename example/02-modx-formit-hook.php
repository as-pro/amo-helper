<?php
use ASPRO\Amo\Manager;
use ASPRO\Amo\Models\Contact;


try {

    if (!$modx) {
        return true;
    }

    $modx->log(xPDO::LOG_LEVEL_ERROR, 'start hook amo');

    $amoDomain = $modx->getOption('amo_domain');
    $amoLogin = $modx->getOption('amo_login');
    $amoApikey = $modx->getOption('amo_apikey');
    $amoResponsible = $modx->getOption('amo_responsible');

    if (!$amoDomain || !$amoLogin || !$amoApikey) {
        $modx->log(xPDO::LOG_LEVEL_ERROR, 'Не заданы параметры доступа к AmoCRM');
        return true;
    }

    $manager = new Manager($amoDomain, $amoLogin, $amoApikey);

    if (!$hook) {
        $modx->log(xPDO::LOG_LEVEL_ERROR, 'hook form not found');
        return true;
    }

    $form = $hook->getValues();
    // $modx->log(xPDO::LOG_LEVEL_ERROR, print_r($form, true));

    $formName = $form['form'] ?? null;

    $phone = $form['phone'] ?? null;
    $email = $form['email'] ?? null;
    $name = $form['name'] ?? null;

    if (!$email && !$phone) {
        $modx->log(xPDO::LOG_LEVEL_ERROR, 'Нет ни телефона, ни емейла');
        return true;
    }

    $pageURL = $form['link'] ?? $form['page_url'] ?? null;
    $price = $form['price'] ?? 0;
    $domain = $modx->getOption('site_name', $_SERVER['SERVER_NAME'] ?? $_SERVER['HTTP_HOST'] ?? null);

    $contact = null;
    if ($phone) {
        $contact = $manager->findContactByPhone($phone);
    }
    if (!$contact && $email) {
        $contact = $manager->findContactByEmail($email);
    }

    if (!$contact) {
        $contact = new Contact();
        if ($amoResponsible) {
            $contact->responsible_user_id = $amoResponsible;
        }
        if ($name) {
            $contact->name = $name;
        }
    }
    if ($phone) {
        $contact->addPhones([$phone]);
    }
    if ($email) {
        $contact->addEmails([$email]);
    }

    $manager->save($contact);

    $deal = $manager->getLastActiveDealForContact($contact);
    if (!$deal) {
        $deal = $contact->newDeal();
        $deal->price = $price;
        if ($formName) {
            $deal->setCustomField('Форма', $formName);
        }
        if ($domain) {
            $deal->setCustomField('Сайт', $domain);
        }
        if ($pageURL) {
            $deal->setCustomField('Страница', $pageURL);
        }
        $manager->save($deal);
    }

    $manager->link($contact, $deal);

    $noteText = json_encode($form, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    $note = $deal->newNote($noteText);
    $task = $deal->newTask('Обработать заявку с сайта ' . $domain);

    $manager->save([$note, $task]);

} catch (\Exception $e) {
    if ($modx) {
        $modx->log(xPDO::LOG_LEVEL_ERROR, $e->getMessage());
        $modx->log(xPDO::LOG_LEVEL_ERROR, $e->getTraceAsString());
    }
}


return true;