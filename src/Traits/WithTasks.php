<?php

namespace ASPRO\Amo\Traits;

use ASPRO\Amo\Models\Task;

trait WithTasks
{
    /**
     * @param string   $text
     * @param int|null $taskType
     * @param int|null $completeTill
     *
     * @return Task
     */
    public function newTask(string $text, ?int $taskType = null, ?int $completeTill = null): Task
    {
        $task = new Task();
        $task->text = $text;
        $task->element_id = $this->id;
        $task->element_type = Task::ELEMENT_TYPE_MAP[get_class($this)] ?? null;
        if ($this->responsible_user_id) {
            $task->responsible_user_id = $this->responsible_user_id;
        }
        if ($taskType) {
            $task->task_type = $taskType;
        }
        if ($completeTill) {
            $task->complete_till = $completeTill;
        }
        return $task;
    }
}