<?php

namespace ASPRO\Amo\Traits;

use ASPRO\Amo\Helper;
use ASPRO\Amo\Models\CustomFiled as cf;

trait WithCustomFields
{
    /**
     * @return array
     */
    public function getCustomFields(): array
    {
        $cf = $this->attributes['custom_fields'] ?? null;
        if (!is_array($cf)) {
            $cf = $this->attributes['custom_fields'] = [];
        }

        return $cf;
    }

    public function sortCustomFields(&$values): void
    {
        foreach (array_keys($values) as $index) {
            $this->sortCustomField($values[$index]);
        }
    }

    public function sortCustomField(&$field): void
    {
        Helper::ksort($field);
        if (isset($field[cf::KEY_VALUES]) && is_array($field[cf::KEY_VALUES])) {
            usort(
                $field[cf::KEY_VALUES],
                function ($a, $b) {
                    $aval = $a[cf::KEY_VALUE] ?? null;
                    $bval = $b[cf::KEY_VALUE] ?? null;
                    if (is_array($aval) || is_array($bval)) {
                        $aval = $aval[cf::KEY_ID] ?? $aval[cf::KEY_CODE] ?? $aval['name'] ?? null;
                        $bval = $bval[cf::KEY_ID] ?? $bval[cf::KEY_CODE] ?? $bval['name'] ?? null;
                    } else {
                        $aval = $aval.($a[cf::KEY_ENUM] ?? '');
                        $bval = $bval.($b[cf::KEY_ENUM] ?? '');
                    }

                    return strcmp($aval, $bval);
                }
            );
        }
    }

    /**
     * @param      $idOrCode
     * @param      $value
     * @param null $enum
     * @param null $subtype
     *
     * @return $this
     */
    public function setCustomField($idOrCode, $value, $enum = null, $subtype = null)
    {
        if (!is_array($value)) {
            $inputs = [[$value, $enum]];
        } else {
            $inputs = $value;
        }

        $values = [];
        foreach ($inputs as $val) {
            if (Helper::isNotAssocArray($val)) {
                list($value, $enum) = $val;
            } else {
                $value = $val;
                $enum = null;
            }

            $fieldValue = [
                cf::KEY_VALUE => $value,
            ];

            if (!is_null($enum)) {
                $fieldValue[cf::KEY_ENUM] = $enum;
            }

            if (!is_null($subtype)) {
                $fieldValue[cf::KEY_SUBTYPE] = $subtype;
            }

            $values[] = $fieldValue;
        }

        $this->setCustomFieldValues($idOrCode, $values);

        return $this;
    }

    /**
     * @param       $idOrCode
     * @param array $values
     *
     * @return $this
     */
    public function setCustomFieldValues($idOrCode, array $values)
    {
        $cf = $this->getCustomFields();
        $index = static::findIndexCustomFieldIn($idOrCode, $cf);
        if ($index === -1) {
            $index = count($this->attributes['custom_fields']);
            $field = [cf::KEY_VALUES => []];
            if (is_numeric($idOrCode)) {
                $field[cf::KEY_ID] = $idOrCode;
            } else {
                $field[cf::KEY_CODE] = $idOrCode;
            }
        } else {
            $field = $this->attributes['custom_fields'][$index];
        }


        $values = $this->fillEnumsFrom($values, $field[cf::KEY_VALUES] ?? []);
        $field[cf::KEY_VALUES] = array_values($values);
        $this->sortCustomField($field);
        $this->attributes['custom_fields'][$index] = $field;

        return $this;
    }

    /**
     * @param array $values
     * @param array $current
     *
     * @return array
     */
    protected function fillEnumsFrom(array $values, array $current)
    {
        foreach ($values as $i => $item) {
            if (isset($item['value']) && !isset($item['enum'])) {
                $enum = $this->findEnumByValue($current, $item['value']);
                if ($enum) {
                    $values[$i]['enum'] = $enum;
                }
            }
        }
        return $values;
    }
    /**
     * @param array $values
     * @param       $value
     *
     * @return int|null
     */
    protected function findEnumByValue(array $values, $value): ?int
    {
        foreach ($values as $val) {
            if (isset($val['value']) && isset($val['enum']) && $val['value'] == $value) {
                return (int)$val['enum'];
            }
        }

        return null;
    }

    /**
     * @param $idOrCode
     *
     * @return array
     */
    public function getCustomFieldValues($idOrCode): array
    {
        $cf = $this->getCustomFields();
        $index = static::findIndexCustomFieldIn($idOrCode, $cf);
        if ($index === -1) {
            return [];
        }

        return $cf[$index][cf::KEY_VALUES] ?? [];
    }

    /**
     * @param             $idOrCode
     * @param string      $keyValue
     * @param string|null $keyIndex
     *
     * @return array
     */
    public function getCustomFieldValuesMap($idOrCode, string $keyValue, ?string $keyIndex = null): array
    {
        $items = $this->getCustomFieldValues($idOrCode);
        $result = [];
        foreach ($items as $index => $item) {
            if ($keyIndex) {
                $index = $item[$keyIndex] ?? null;
            }
            $value = $item[$keyValue] ?? null;
            if (!is_null($index)) {
                $result[$index] = $value;
            } else {
                $result[] = $value;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    protected function getDirtyCustomFields()
    {
        $dirty = [];
        $cf = $this->attributes['custom_fields'] ?? [];
        $cfOriginal = $this->original['custom_fields'] ?? [];
        foreach ($cf as $field) {
            if (is_array($field)) {
                $idOrCode = $field[cf::KEY_ID] ?? $field[cf::KEY_CODE] ?? null;
                if ($idOrCode) {
                    $fieldOriginal = static::getCustomFieldFrom($idOrCode, $cfOriginal);
                    if (!Helper::isEquals($field, $fieldOriginal)) {
                        $dirty[] = $field;
                    }
                }
            }
        }

        return empty($dirty) ? null : $dirty;
    }

    /**
     * @param       $idOrCode
     * @param array $cf
     *
     * @return int|string
     */
    protected static function findIndexCustomFieldIn($idOrCode, array $cf)
    {
        foreach ($cf as $index => $item) {
            if (is_numeric($idOrCode)) {
                if (($item[cf::KEY_ID] ?? null) == $idOrCode) {
                    return $index;
                }
            } else {
                if (($item[cf::KEY_CODE] ?? null) == $idOrCode) {
                    return $index;
                } elseif (($item[cf::KEY_NAME] ?? null) == $idOrCode) {
                    return $index;
                }
            }
        }

        return -1;
    }

    /**
     * @param       $idOrCode
     * @param array $cf
     *
     * @return array|mixed
     */
    protected static function getCustomFieldFrom($idOrCode, array $cf)
    {
        $index = static::findIndexCustomFieldIn($idOrCode, $cf);
        if ($index === -1) {
            $field = [];
            if (is_numeric($idOrCode)) {
                $field[cf::KEY_ID] = $idOrCode;
            } else {
                $field[cf::KEY_CODE] = $idOrCode;
            }
        } else {
            $field = $cf[$index];
        }

        return $field;
    }

    /**
     * @param array $cf
     * @param array $cfOriginal
     * @param bool  $firstOnly
     *
     * @return array
     */
    protected static function getDiffCustomFields(array $cf, array $cfOriginal, bool $firstOnly = false): array
    {
        $dirty = [];
        foreach ($cf as $field) {
            $idOrCode = $field[cf::KEY_ID] ?? $field[cf::KEY_CODE] ?? null;
            if ($idOrCode) {
                $fieldOriginal = static::getCustomFieldFrom($idOrCode, $cfOriginal);
                if (json_encode($field) !== json_encode($fieldOriginal)) {
                    $dirty[] = $field;
                    if ($firstOnly) {
                        return $dirty;
                    }
                }
            }
        }

        return $dirty;
    }
}