<?php

namespace ASPRO\Amo\Traits;

use ASPRO\Amo\Models\Note;

trait WithNotes
{
    /**
     * @param string $text
     * @param int    $noteType
     *
     * @return Note
     */
    public function newNote(string $text, int $noteType = Note::TYPE_COMMON): Note
    {
        $task = new Note();
        $task->text = $text;
        $task->element_id = $this->id;
        $task->element_type = Note::ELEMENT_TYPE_MAP[get_class($this)] ?? null;
        $task->note_type = $noteType;
        if ($this->responsible_user_id) {
            $task->responsible_user_id = $this->responsible_user_id;
        }
        return $task;
    }
}