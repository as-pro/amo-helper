<?php

namespace ASPRO\Amo;

use AmoCRM\Client;
use AmoCRM\Exception;
use ASPRO\Amo\Models\AbstractModel;
use ASPRO\Amo\Models\Contact;
use ASPRO\Amo\Models\CustomFiled;
use ASPRO\Amo\Models\Deal;
use ASPRO\Amo\Models\Note;
use ASPRO\Amo\Models\Task;
use Psr\SimpleCache\CacheInterface;

class Manager
{
    private const ENTITIES_MAP = [
        Contact::class => 'contacts',
        Deal::class    => 'leads',
        Task::class    => 'tasks',
        Note::class    => 'notes',
    ];

    private const SET_URL_MAP = [
        Contact::class => '/private/api/v2/json/contacts/set',
        Deal::class    => '/private/api/v2/json/leads/set',
        Task::class    => '/private/api/v2/json/tasks/set',
        Note::class    => '/private/api/v2/json/notes/set',
    ];

    private $amo;
    private $debug;

    /**
     * @var array|null;
     */
    private $accountInfo;

    /**
     * @var CacheInterface|null
     */
    private $cahce;

    /**
     * @var string
     */
    private $cahcePrefix;

    /**
     * @var int
     */
    private $cahceTTL = 60 * 60;

    /**
     * Manager constructor.
     *
     * @param string $domain
     * @param string $login
     * @param string $apikey
     * @param null   $proxy
     * @param bool   $debug
     */
    public function __construct(string $domain, string $login, string $apikey, $proxy = null, bool $debug = false)
    {
        $this->amo = new Client($domain, $login, $apikey, $proxy);
        $this->debug = $debug;
        $this->cahcePrefix = md5($domain.$login);
    }

    /**
     * @param CacheInterface|null $cache
     */
    public function setCache(?CacheInterface $cache): void
    {
        $this->cahce = $cache;
    }

    /**
     * @return Client
     */
    public function getClient(): Client
    {
        $this->amo->parameters->clearPost()->clearGet();

        return $this->amo;
    }

    /**
     * @return Request
     */
    public function newRequest(): Request
    {
        return Request::create($this->getClient())->debug($this->debug);
    }

    /**
     * @return array
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getAccountInfo(): array
    {
        if (null !== $this->accountInfo) {
            return $this->accountInfo;
        }

        $cacheKey = $this->cahcePrefix.'_account_info';
        if (null !== $this->cahce) {
            if ($this->cahce->has($cacheKey)) {
                $this->accountInfo = $this->cahce->get($cacheKey);

                return $this->accountInfo;
            }
        }

        $accountInfo = $this->getClient()->account->debug($this->debug)->apiCurrent();
        if (empty($accountInfo) || !is_array($accountInfo)) {
            throw new \Exception('Could not get amo account info');
        }

        $this->accountInfo = $accountInfo;
        if (null !== $this->cahce) {
            $this->cahce->set($cacheKey, $accountInfo, $this->cahceTTL);
        }

        return $this->accountInfo;
    }

    /**
     * @param array $parameters
     * @param null  $modified
     *
     * @return array|Contact[]
     */
    public function findContacts(array $parameters, $modified = null)
    {
        $items = $this->amo->contact->debug($this->debug)->apiList($parameters, $modified);

        $result = [];
        foreach ($items as $item) {
            $item = new Contact($item);
            $result[$item->id] = $item;
        }

        return $result;
    }

    /**
     * @param array $parameters
     * @param null  $modified
     *
     * @return Contact|null
     */
    public function findOneContact(array $parameters, $modified = null): ?Contact
    {
        $parameters['limit'] = 1;
        $items = $this->findContacts($parameters, $modified);
        foreach ($items as $item) {
            return $item;
        }

        return null;
    }

    /**
     * @param int $id
     *
     * @return Contact|mixed|null
     */
    public function findContactByID(int $id): ?Contact
    {
        $items = $this->findContacts(['id' => $id]);

        return $items[$id] ?? null;
    }

    /**
     * @param string $phone
     *
     * @return Contact|null
     */
    public function findContactByPhone(string $phone): ?Contact
    {
        $items = $this->findContacts(['query' => Helper::cleanPhone($phone), 'limit' => 3]);
        foreach ($items as $item) {
            if ($item->hasPhone($phone)) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param string $email
     *
     * @return Contact|null
     */
    public function findContactByEmail(string $email): ?Contact
    {
        $items = $this->findContacts(['query' => $email, 'limit' => 3]);
        foreach ($items as $item) {
            if ($item->hasEmail($email)) {
                return $item;
            }
        }

        return null;
    }

    /**
     * @param array $parameters
     * @param null  $modified
     *
     * @return array|Deal[]
     */
    public function findDeals(array $parameters, $modified = null)
    {
        $items = $this->amo->lead->debug($this->debug)->apiList($parameters, $modified);

        $result = [];
        foreach ($items as $item) {
            $item = new Deal($item);
            $result[$item->id] = $item;
        }

        return $result;
    }

    /**
     * @param array $parameters
     * @param null  $modified
     *
     * @return Deal|null
     */
    public function findOneDeal(array $parameters, $modified = null): ?Deal
    {
        $parameters['limit'] = 1;
        $items = $this->findDeals($parameters, $modified);
        foreach ($items as $item) {
            return $item;
        }

        return null;
    }

    /**
     * @param int $id
     *
     * @return Deal|null
     */
    public function findDealByID(int $id): ?Deal
    {
        $items = $this->findDeals(['id' => $id]);

        return $items[$id] ?? null;
    }

    /**
     * @param Contact $contact
     *
     * @return Deal|null
     */
    public function getLastActiveDealForContact(Contact $contact): ?Deal
    {
        $dealIDs = $contact->linked_leads_id;
        if (!empty($dealIDs)) {
            $deals = $this->findDeals(['id' => $dealIDs]);
            $found = null;
            foreach ($deals as $deal) {
                if ($deal->isActive()) {
                    if (!$found) {
                        $found = $deal;
                    }
                    if ($deal->last_modified > $found->last_modified) {
                        $found = $deal;
                    }
                }
            }

            return $found;
        }

        return null;
    }

    /**
     * @param $entities
     *
     * @throws \AmoCRM\Exception
     * @throws \AmoCRM\NetworkException
     */
    public function save($entities): void
    {
        if (!is_iterable($entities)) {
            $entities = [$entities];
        }
        $class = null;
        $items = [];
        foreach ($entities as $entity) {
            if (is_object($entity)) {
                $currentClass = get_class($entity);
                if (!$class) {
                    $class = $currentClass;
                } elseif ($class !== $currentClass) {
                    $this->saveEntities($class, $items);
                    $class = $currentClass;
                    $items = [];
                }
                $items[] = $entity;
            }
        }

        if (!empty($items)) {
            $this->saveEntities($class, $items);
        }
    }

    /**
     * @param string     $class
     * @param iterable   $entities
     * @param array|null $keys
     *
     * @throws \AmoCRM\Exception
     * @throws \AmoCRM\NetworkException
     */
    protected function saveEntities(string $class, iterable $entities, ?array $keys = null): void
    {
        $url = self::SET_URL_MAP[$class] ?? null;
        if (!$url) {
            throw new \InvalidArgumentException('unknown entity: '.$class);
        }
        $entityName = self::ENTITIES_MAP[$class];

        $update = [];
        $add = [];

        /** @var AbstractModel[] $map */
        $map = [];
        $requestID = rand(999999, 9999999);
        foreach ($entities as $entity) {
            if ($entity instanceof AbstractModel) {
                if ($entity->getID()) {
                    $attributes = $entity->getDirty($keys);
                    if (!empty($attributes)) {
                        $map[$entity->getID()] = $entity;
                        $attributes['id'] = $entity->getID();
                        if (!isset($attributes['last_modified'])) {
                            $attributes['last_modified'] = time();
                        }
                        $this->prepareAttributes($class, $attributes);
                        $update[] = $attributes;
                    }
                } else {
                    $attributes = $entity->getAttributes();
                    if (!isset($attributes['request_id'])) {
                        $attributes['request_id'] = ++$requestID;
                    }
                    $map['request_'.$attributes['request_id']] = $entity;
                    $this->prepareAttributes($class, $attributes);
                    $add[] = $attributes;
                }
            }
        }

        if (empty($add) && empty($update)) {
            return;
        }

        $parameters = [
            $entityName => [
                'add'    => $add,
                'update' => $update,
            ],
        ];

        $response = $this->newRequest()->post($url, $parameters);
        $ts = $response['server_time'] ?? time();
        if (isset($response[$entityName]['add'])) {
            foreach ($response[$entityName]['add'] as $item) {
                $mapKey = 'request_'.($item['request_id'] ?? '');
                if (isset($map[$mapKey])) {
                    $map[$mapKey]->setRawAttributes(
                        [
                            'id'            => $item['id'],
                            'last_modified' => $ts,
                        ]
                    );
                    $map[$mapKey]->syncAttributes(['id', 'last_modified']);
                }
            }
        }

        $error = $response[$entityName]['add'][0]['error'] ?? $response[$entityName]['update'][0]['error'] ?? null;
        if ($error) {
            throw new Exception($error);
        }

        foreach ($map as $item) {
            $item->syncAttributes($keys);
        }
    }

    protected function prepareAttributes(string $entityType, array &$attributes): void
    {
        if (isset($attributes['custom_fields'])) {
            $this->prepareCustomFields($entityType, $attributes['custom_fields']);
        }
    }

    protected function prepareCustomFields(string $entityType, array &$customFields): void
    {
        foreach ($customFields as $key => &$cf) {
            $cfID = $cf[CustomFiled::KEY_ID] ?? null;
            if ($cfID === null) {
                $codeOrName = $cf[CustomFiled::KEY_CODE] ?? $cf[CustomFiled::KEY_NAME] ?? null;
                if (!is_null($codeOrName)) {
                    $cfID = $this->getCustomFieldId($codeOrName, $entityType);
                    if (!is_null($cfID)) {
                        $cf[CustomFiled::KEY_ID] = $cfID;
                    }
                }
            }

            if ($cfID && isset($cf[CustomFiled::KEY_VALUES])) {
                foreach ($cf[CustomFiled::KEY_VALUES] as &$value) {
                    $enum = $value[CustomFiled::KEY_ENUM] ?? null;
                    if (!is_numeric($enum)) {
                        $enumID =
                            $this->getCustomFieldEnumId(
                                $cfID,
                                $enum ?? $value[CustomFiled::KEY_VALUE] ?? '',
                                $entityType
                            );
                        if (is_null($enumID)) {
                            $enumID = $this->getCustomFieldDefaultEnumId($cfID, $entityType);
                        }
                        if (!is_null($enumID)) {
                            $value[CustomFiled::KEY_ENUM] = $enumID;
                        }
                    }
                }
            }

            unset($cf[CustomFiled::KEY_NAME], $cf[CustomFiled::KEY_CODE]);
        }
    }

    /**
     * @param Contact $contact
     * @param Deal    $deal
     *
     * @throws Exception
     * @throws \AmoCRM\NetworkException
     */
    public function link(Contact $contact, Deal $deal): void
    {
        if (!$contact->getID()) {
            $this->save($contact);
        }
        if (!$deal->getID()) {
            $this->save($deal);
        }
        if (!$contact->hasDeal($deal)) {
            $link = $this->amo->links;
            $link['from'] = 'leads';
            $link['from_id'] = $deal->getID();
            $link['to'] = 'contacts';
            $link['to_id'] = $contact->getID();
            $link->apiLink();
        }
    }

    /**
     * @param Contact $contact
     * @param Deal    $deal
     */
    public function unlink(Contact $contact, Deal $deal): void
    {
        if (!$contact->getID()) {
            return;
        }
        if (!$deal->getID()) {
            return;
        }
        if ($contact->hasDeal($deal)) {
            $link = $this->amo->links;
            $link['from'] = 'leads';
            $link['from_id'] = $deal->getID();
            $link['to'] = 'contacts';
            $link['to_id'] = $contact->getID();
            $link->apiUnLink();
        }
    }

    /**
     * @param             $idOrCodeOrName
     * @param string|null $entityType
     *
     * @return array|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCustomField($idOrCodeOrName, ?string $entityType = null): ?array
    {
        $cfsByGroup = $this->getAccountInfo()['custom_fields'] ?? [];

        if (!is_null($entityType)) {
            $entityType = self::ENTITIES_MAP[$entityType] ?? $entityType;
        }

        foreach ($cfsByGroup as $entity => $cfs) {
            foreach ($cfs as $cf) {
                if (!is_null($entityType) && $entityType !== $entity) {
                    continue;
                }
                if (is_numeric($idOrCodeOrName)) {
                    if (intval($cf[CustomFiled::KEY_ID] ?? 0) === intval($idOrCodeOrName)) {
                        return $cf;
                    }
                } else {
                    if (Helper::equalsCode($cf[CustomFiled::KEY_CODE] ?? '', $idOrCodeOrName)) {
                        return $cf;
                    } elseif (Helper::equalsCode($cf[CustomFiled::KEY_NAME] ?? '', $idOrCodeOrName)) {
                        return $cf;
                    }
                }
            }
        }

        return null;
    }

    /**
     * @param string      $codeOrName
     * @param string|null $entityType
     *
     * @return int|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCustomFieldId(string $codeOrName, ?string $entityType = null): ?int
    {
        $cf = $this->getCustomField($codeOrName, $entityType) ?? [];

        return $cf[CustomFiled::KEY_ID] ?? null;
    }

    /**
     * @param             $idOrCodeOrName
     * @param string      $enum
     * @param string|null $entityType
     *
     * @return int|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCustomFieldEnumId($idOrCodeOrName, string $enum, ?string $entityType = null): ?int
    {
        $enums = $this->getCustomField($idOrCodeOrName, $entityType)[CustomFiled::KEY_ENUMS] ?? [];
        foreach ($enums as $id => $val) {
            if (Helper::equalsCode($val, $enum)) {
                return intval($id);
            }
        }

        return null;
    }

    /**
     * @param             $idOrCodeOrName
     * @param string|null $entityType
     *
     * @return int|null
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function getCustomFieldDefaultEnumId($idOrCodeOrName, ?string $entityType = null): ?int
    {
        $enums = $this->getCustomField($idOrCodeOrName, $entityType)[CustomFiled::KEY_ENUMS] ?? [];
        foreach ($enums as $id => $val) {
            return intval($id);
        }

        return null;
    }
}