<?php

namespace ASPRO\Amo;

class Helper
{
    /**
     * @param $phone
     *
     * @return string
     */
    public static function cleanPhone($phone): string
    {
        return preg_replace('/[^\d]+/', '', strval($phone));
    }

    /**
     * @param $a
     * @param $b
     *
     * @return bool
     */
    public static function equalsPhones($a, $b): bool
    {
        return static::cleanPhone($a) === static::cleanPhone($b);
    }

    /**
     * @param     $array
     * @param int $sort_flags
     *
     * @return bool
     */
    public static function ksort(&$array, $sort_flags = SORT_REGULAR): bool
    {
        if (!is_array($array)) {
            return false;
        }
        ksort($array, $sort_flags);
        foreach ($array as &$arr) {
            static::ksort($arr, $sort_flags);
        }

        return true;
    }

    /**
     * @param $a
     * @param $b
     *
     * @return bool
     */
    public static function isEquals($a, $b): bool
    {
        if ($a === $b) {
            return true;
        }
        if (is_scalar($a) || is_scalar($b)) {
            return $a == $b;
        }
        if (is_array($a) && is_array($b)) {
            if (count($a) !== count($b)) {
                return false;
            }
            foreach ($a as $index => $aval) {
                $bval = $b[$index] ?? null;
                if (!static::isEquals($aval, $bval)) {
                    return false;
                }
            }

            return true;
        }

        return $a === $b;
    }

    /**
     * @param $array
     *
     * @return bool
     */
    public static function isAssocArray($array): bool
    {
        if (!is_array($array)) {
            return false;
        }
        if ([] === $array) {
            return false;
        }

        return array_keys($array) !== range(0, count($array) - 1);
    }

    public static function isNotAssocArray($array): bool
    {
        return is_array($array) && !self::isAssocArray($array);
    }

    public static function equalsCode(string $a, string $b): bool
    {
        return trim(mb_strtolower($a)) === trim(mb_strtolower($b));
    }
}