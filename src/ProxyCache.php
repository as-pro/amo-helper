<?php

namespace ASPRO\Amo;

use Psr\SimpleCache\CacheInterface;

class ProxyCache implements CacheInterface
{
    /**
     * @var callable
     */
    private $getter;

    /**
     * @var callable
     */
    private $setter;

    /**
     * @var callable
     */
    private $hasser;

    /**
     * @var callable
     */
    private $remover;

    /**
     * @var callable
     */
    private $clear;

    /**
     * ProxyCache constructor.
     *
     * @param callable $getter
     * @param callable $setter
     * @param callable $hasser
     * @param callable $remover
     * @param callable $clear
     */
    public function __construct(
        callable $getter,
        callable $setter,
        callable $hasser,
        callable $remover,
        callable $clear
    ) {
        $this->getter = $getter;
        $this->setter = $setter;
        $this->hasser = $hasser;
        $this->remover = $remover;
        $this->clear = $clear;
    }

    public static function fromObject($object): self
    {
        if (!is_object($object)) {
            throw new \InvalidArgumentException();
        }

        $methods = [];

        if (method_exists($object, 'get')) {
            $methods['getter'] = [$object, 'get'];
        }

        if (method_exists($object, 'set')) {
            $methods['setter'] = [$object, 'set'];
        }

        if (method_exists($object, 'has')) {
            $methods['hasser'] = [$object, 'has'];
        } elseif (isset($methods['getter'])) {
            $getter = $methods['getter'];
            $methods['hasser'] = function ($key) use($getter) {
                return call_user_func($getter, $key) !== null;
            };
        }

        if (method_exists($object, 'delete')) {
            $methods['remover'] = [$object, 'delete'];
        } elseif (method_exists($object, 'remove')) {
            $methods['remover'] = [$object, 'remove'];
        }

        if (method_exists($object, 'clear')) {
            $methods['clear'] = [$object, 'clear'];
        } elseif (method_exists($object, 'clean')) {
            $methods['clear'] = [$object, 'clean'];
        } elseif (method_exists($object, 'flush')) {
            $methods['clear'] = [$object, 'flush'];
        }

        if (isset($methods['getter'], $methods['setter'], $methods['remover'], $methods['hasser'], $methods['clear'])) {
            return new static(
                $methods['getter'],
                $methods['setter'],
                $methods['hasser'],
                $methods['remover'],
                $methods['clear']
            );
        }

        throw new \InvalidArgumentException();
    }

    public function get($key, $default = null)
    {
        return call_user_func($this->getter, $key, $default);
    }

    public function set($key, $value, $ttl = null)
    {
        return call_user_func($this->setter, $key, $value, $ttl);
    }

    public function delete($key)
    {
        return call_user_func($this->remover, $key);
    }

    public function has($key)
    {
        return call_user_func($this->hasser, $key);
    }

    public function clear()
    {
        return call_user_func($this->clear);
    }

    public function getMultiple($keys, $default = null)
    {
        foreach ($keys as $key) {
            yield $key => $this->get($key, $default);
        }
    }

    public function setMultiple($values, $ttl = null)
    {
        foreach ($values as $key => $value) {
            if (!$this->set($key, $value, $ttl)) {
                return false;
            }
        }
        return true;
    }

    public function deleteMultiple($keys)
    {
        foreach ($keys as $key) {
            if (!$this->delete($key)) {
                return false;
            }
        }
        return true;
    }

}