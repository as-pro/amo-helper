<?php

namespace ASPRO\Amo\Models;

use ASPRO\Amo\Helper;

abstract class AbstractModel implements \ArrayAccess
{
    protected $attributes = [];
    protected $original   = [];
    protected $fields     = [];

    /**
     * AbstractModel constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->setRawAttributes($attributes);
        $this->syncAttributes();
    }

    /**
     * @return int|null
     */
    public function getID(): ?int
    {
        $id = $this->attributes['id'] ?? null;

        return is_null($id) ? $id : intval($id);
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    protected function sortAttributes(array &$attributes): void
    {
        ksort($attributes);
        foreach ($attributes as $key => $value) {
            if (is_array($value)) {
                $method = 'sort'.str_replace('_', '', ucwords($key, '_'));
                if (method_exists($this, $method)) {
                    $this->{$method}($attributes[$key]);
                } else {
                    Helper::ksort($attributes[$key]);
                }
            }
        }
    }

    /**
     * @param null $keys
     */
    public function syncAttributes($keys = null): void
    {
        if (is_null($keys)) {
            $this->original = $this->attributes;
        } elseif (is_string($keys)) {
            $this->original[$keys] = $this->attributes[$keys] ?? null;
        } elseif (is_array($keys)) {
            foreach ($keys as $key) {
                $this->original[$key] = $this->attributes[$key] ?? null;
            }
        }
    }

    /**
     * @param array|null $keys
     *
     * @return array
     */
    public function getDirty(?array $keys = null): array
    {
        $dirty = [];
        $keys = $keys ?? array_keys($this->attributes);
        foreach ($keys as $key) {
            $method = 'getDirty'.str_replace('_', '', ucwords($key, '_'));
            if (method_exists($this, $method)) {
                $v = $this->{$method}();
                if (!is_null($v)) {
                    $dirty[$key] = $v;
                }
            } else {
                if (!$this->isEqualAttribute($key, $this->attributes[$key] ?? null, $this->original[$key] ?? null)) {
                    $dirty[$key] = $this->attributes[$key] ?? null;
                }
            }
        }

        return $dirty;
    }

    /**
     * @param string $key
     * @param        $value
     * @param        $original
     *
     * @return bool
     */
    protected function isEqualAttribute(string $key, $value, $original): bool
    {
        $method = 'isEqual'.str_replace('_', '', ucwords($key, '_'));
        if (method_exists($this, $method)) {
            return $this->{$method}($key, $value, $original);
        }
        if ($value === $original) {
            return true;
        }
        if (is_scalar($original)) {
            return $value == $original;
        } else {
            return json_encode($original) === json_encode($value);
        }
    }

    /**
     * @param array $attributes
     * @param bool  $reset
     */
    public function setRawAttributes(array $attributes, bool $reset = false): void
    {
        if ($reset) {
            $this->attributes = [];
        }
        foreach ($attributes as $key => $val) {
            $this->attributes[$key] = $val;
        }
        $this->sortAttributes($this->attributes);
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function offsetExists($key)
    {
        return isset($this->attributes[$key]);
    }

    /**
     * @param string $key
     *
     * @return mixed|null
     */
    public function offsetGet($key)
    {
        $method = 'get'.$key;
        if (method_exists($this, $method)) {
            return $this->{$method}();
        } elseif (in_array($key, $this->fields)) {
            return $this->attributes[$key] ?? null;
        }

        return null;
    }

    /**
     * @param string $key
     * @param mixed  $value
     */
    public function offsetSet($key, $value)
    {
        $method = 'set'.$key;
        if (method_exists($this, $method)) {
            $this->{$method}($value);
        } elseif (in_array($key, $this->fields)) {
            $this->attributes[$key] = $value;
        }
    }

    /**
     * @param string $key
     */
    public function offsetUnset($key)
    {
        unset($this->attributes[$key]);
    }

    /**
     * @param $name
     *
     * @return mixed|null
     */
    public function __get($name)
    {
        return $this->offsetGet($name);
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        $this->offsetSet($name, $value);
    }

}