<?php

namespace ASPRO\Amo\Models;

/**
 * @property $element_id
 * @property $element_type
 * @property $note_type
 * @property $date_create
 * @property $last_modified
 * @property $request_id
 * @property $text
 * @property $responsible_user_id
 * @property $created_user_id
 *
 **/
class Note extends AbstractModel
{
    const TYPE_COMMON      = 4;
    const TYPE_TASK_RESULT = 13;
    const TYPE_SYSTEM      = 25;
    const TYPE_SMS_IN      = 102;
    const TYPE_SMS_OUT     = 103;

    const ELEMENT_TYPE_MAP = [
        Contact::class => 1,
        Deal::class    => 2,
        Task::class    => 4,
    ];

    protected $fields = [
        'element_id',
        'element_type',
        'note_type',
        'date_create',
        'last_modified',
        'request_id',
        'text',
        'responsible_user_id',
        'created_user_id',
    ];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!$this->note_type) {
            $this->note_type = self::TYPE_COMMON;
        }
    }
}