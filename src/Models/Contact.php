<?php

namespace ASPRO\Amo\Models;

use ASPRO\Amo\Helper;
use ASPRO\Amo\Traits\WithCustomFields;
use ASPRO\Amo\Traits\WithNotes;
use ASPRO\Amo\Traits\WithTasks;

/**
 * @property $id
 * @property $name
 * @property $request_id
 * @property $date_create
 * @property $last_modified
 * @property $responsible_user_id
 * @property $created_user_id
 * @property $linked_leads_id
 * @property $company_name
 * @property $linked_company_id
 * @property $tags
 * @property $notes
 * @property $modified_user_id
 */
class Contact extends AbstractModel
{
    use WithCustomFields, WithTasks, WithNotes;

    const CF_PHONE_CODE         = 'PHONE';
    const CF_EMAIL_CODE         = 'EMAIL';
    const CF_PHONE_DEFAULT_ENUM = 'MOB';
    const CF_EMAIL_DEFAULT_ENUM = 'PRIV';

    protected $fields = [
        'name',
        'request_id',
        'date_create',
        'last_modified',
        'responsible_user_id',
        'created_user_id',
        'linked_leads_id',
        'company_name',
        'linked_company_id',
        'tags',
        'notes',
        'modified_user_id',
    ];

    /**
     * Contact constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!$this->name) {
            $this->name = 'New contact';
        }
    }

    /**
     * @return Deal
     */
    public function newDeal(): Deal
    {
        $deal = new Deal();
        if ($this->responsible_user_id) {
            $deal->responsible_user_id = $this->responsible_user_id;
        }
        if ($this->getID()) {
            $deal->main_contact_id = $this->getID();
        }

        return $deal;
    }

    /**
     * @return array
     */
    public function getPhones(): array
    {
        return $this->getCustomFieldValuesMap(self::CF_PHONE_CODE, 'value');
    }

    /**
     * @param string $phone
     *
     * @return bool
     */
    public function hasPhone(string $phone): bool
    {
        $clPhone = Helper::cleanPhone($phone);

        return array_key_exists($clPhone, $this->getPhonesValues());
    }

    /**
     * @return array
     */
    public function getPhonesValues(): array
    {
        $values = $this->getCustomFieldValues(self::CF_PHONE_CODE) ?? [];
        $result = [];
        foreach ($values as $item) {
            $phone = strval($item['value'] ?? '');
            $clPhone = Helper::cleanPhone($phone);
            $enum = intval($item['enum'] ?? 0);
            if ($clPhone) {
                $result[$clPhone] = [
                    'enum'  => $enum,
                    'value' => $phone,
                ];
            }
        }

        return $result;
    }

    /**
     * @param array $phones
     * @param bool  $reset
     */
    public function addPhones(array $phones, bool $reset = false)
    {
        $current = $this->getPhonesValues();
        $values = [];
        foreach ($phones as $phone) {
            $clPhone = Helper::cleanPhone($phone);
            if (array_key_exists($clPhone, $current)) {
                $values[$clPhone] = $current[$clPhone];
            } else {
                $values[$clPhone] = [
                    'value' => $phone,
                    'enum'  => self::CF_PHONE_DEFAULT_ENUM,
                ];
            }
        }
        if (!$reset) {
            $values += $current;
        }
        $this->setCustomFieldValues(self::CF_PHONE_CODE, array_values($values));
    }

    /**
     * @return array
     */
    public function getEmails(): array
    {
        return $this->getCustomFieldValuesMap(self::CF_EMAIL_CODE, 'value');
    }

    /**
     * @param string $email
     *
     * @return bool
     */
    public function hasEmail(string $email): bool
    {
        return in_array($email, $this->getEmails());
    }

    /**
     * @return array
     */
    public function getEmailValues(): array
    {
        $values = $this->getCustomFieldValues(self::CF_EMAIL_CODE) ?? [];
        $result = [];
        foreach ($values as $item) {
            $email = strval($item['value'] ?? '');
            $enum = intval($item['enum'] ?? 0);
            $result[$email] = [
                'enum'  => $enum,
                'value' => $email,
            ];
        }

        return $result;
    }

    /**
     * @param array $emails
     * @param bool  $reset
     */
    public function addEmails(array $emails, bool $reset = false)
    {
        $current = $this->getEmailValues();
        $values = [];
        foreach ($emails as $email) {
            if (array_key_exists($email, $current)) {
                $values[$email] = $current[$email];
            } else {
                $values[$email] = [
                    'value' => $email,
                    'enum'  => self::CF_EMAIL_DEFAULT_ENUM,
                ];
            }
        }
        if (!$reset) {
            $values += $current;
        }
        $this->setCustomFieldValues(self::CF_EMAIL_CODE, array_values($values));
    }

    /**
     * @param $deal
     *
     * @return bool
     */
    public function hasDeal($deal)
    {
        if ($deal instanceof Deal) {
            $deal = $deal->getID();
        }

        if (!is_numeric($deal) || !is_array($this->linked_leads_id)) {
            return false;
        }

        return in_array($deal, $this->linked_leads_id);
    }
}