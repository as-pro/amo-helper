<?php

namespace ASPRO\Amo\Models;

use ASPRO\Amo\Traits\WithCustomFields;
use ASPRO\Amo\Traits\WithNotes;
use ASPRO\Amo\Traits\WithTasks;

/**
 * @property $id
 * @property $name
 * @property $date_create
 * @property $last_modified
 * @property $status_id
 * @property $pipeline_id
 * @property $price
 * @property $responsible_user_id
 * @property $created_user_id
 * @property $request_id
 * @property $linked_company_id
 * @property $tags
 * @property $visitor_uid
 * @property $notes
 * @property $modified_user_id
 * @property $main_contact_id
 */
class Deal extends AbstractModel
{
    use WithCustomFields, WithTasks, WithNotes;

    const STATUS_SUCCESS = 142;
    const STATUS_FAILED  = 143;

    protected $fields = [
        'name',
        'date_create',
        'last_modified',
        'status_id',
        'pipeline_id',
        'price',
        'responsible_user_id',
        'created_user_id',
        'request_id',
        'linked_company_id',
        'tags',
        'visitor_uid',
        'notes',
        'modified_user_id',
        'main_contact_id',
    ];

    /**
     * Deal constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!$this->name) {
            $this->name = 'New deal';
        }
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        $statusID = (int)$this->status_id;
        if ($statusID) {
            return $statusID !== self::STATUS_FAILED && $statusID !== self::STATUS_SUCCESS;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->status_id == self::STATUS_SUCCESS;
    }

    /**
     * @return bool
     */
    public function isFailed(): bool
    {
        return $this->status_id == self::STATUS_FAILED;
    }
}