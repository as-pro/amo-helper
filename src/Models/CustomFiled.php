<?php

namespace ASPRO\Amo\Models;

final class CustomFiled
{
    const KEY_ID      = 'id';
    const KEY_CODE    = 'code';
    const KEY_NAME    = 'name';
    const KEY_SUBTYPE = 'subtype';
    const KEY_ENUM    = 'enum';
    const KEY_ENUMS   = 'enums';
    const KEY_VALUE   = 'value';
    const KEY_VALUES  = 'values';
}