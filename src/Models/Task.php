<?php

namespace ASPRO\Amo\Models;

/**
 * @property $element_id
 * @property $element_type
 * @property $date_create
 * @property $last_modified
 * @property $status
 * @property $request_id
 * @property $task_type
 * @property $text
 * @property $responsible_user_id
 * @property $complete_till
 * @property $created_user_id
 */
class Task extends AbstractModel
{
    const TYPE_CALL        = 1;
    const TYPE_APPOINTMENT = 2;
    const TYPE_LETTER      = 3;

    const ELEMENT_TYPE_MAP = [
        Contact::class => 1,
        Deal::class => 2,
    ];

    protected $fields = [
        'element_id',
        'element_type',
        'date_create',
        'last_modified',
        'status',
        'request_id',
        'task_type',
        'text',
        'responsible_user_id',
        'complete_till',
        'created_user_id',
    ];

    /**
     * Task constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        if (!$this->complete_till) {
            $this->complete_till = time();
        }
        if (!$this->task_type) {
            $this->task_type = self::TYPE_CALL;
        }
    }
}