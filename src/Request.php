<?php

namespace ASPRO\Amo;

use AmoCRM\Client;

class Request extends \AmoCRM\Request\Request
{
    /**
     * @param Client $client
     *
     * @return Request
     */
    public static function create(Client $client): Request
    {
        return \Closure::bind(
            function () {
                return new Request($this->parameters, $this->curlHandle);
            },
            $client,
            Client::class
        )->__invoke();
    }

    /**
     * @param       $url
     * @param array $parameters
     *
     * @return mixed
     * @throws \AmoCRM\Exception
     * @throws \AmoCRM\NetworkException
     */
    public function post($url, $parameters = [])
    {
        $p = $this->getParameters();
        if ($p) {
            $p->clearGet()->clearPost();
        }

        return $this->postRequest($url, $parameters);
    }

    /**
     * @param       $url
     * @param array $parameters
     * @param null  $modified
     *
     * @return mixed
     * @throws \AmoCRM\Exception
     * @throws \AmoCRM\NetworkException
     */
    public function get($url, $parameters = [], $modified = null)
    {
        $p = $this->getParameters();
        if ($p) {
            $p->clearGet()->clearPost();
        }

        return $this->getRequest($url, $parameters, $modified);
    }
}